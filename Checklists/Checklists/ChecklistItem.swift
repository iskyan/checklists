//
//  ChecklistItem.swift
//  Checklists
//
//  Created by Feruza Atahodjaeva on 2/28/19.
//  Copyright © 2019 Home. All rights reserved.
//

import Foundation

class ChecklistItem: NSObject {
    
    var text = ""
    var checked = false
    
    func toggleChecked()  {
        checked = !checked
    }
}
